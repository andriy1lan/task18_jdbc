package service;
import java.util.List;


import dao.DepartmentDao;

import model.Department;

public abstract class DepartmentService extends Service{
	  
	  DepartmentDao depDAO=new DepartmentDao();
	
	  public void insert(Department obj) {depDAO.insert(obj);}
	  public Department getById(int id) {return (Department)depDAO.getById(id);}
	  public void update(Department obj){depDAO.update(obj);}
	  public void delete(int id){depDAO.delete(id);}
	  public List<Department> getAll() {return (List<Department>)depDAO.getAll();}

}