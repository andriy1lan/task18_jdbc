package service;
import java.util.List;

public abstract class Service {
	
	  public abstract void insert(Object obj);
	  public abstract Object getById(int id);
	  public abstract void update(Object obj);
	  public abstract void delete(int id);
	  public abstract List<?> getAll();

}