package connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	static Connection conn=null;
	static public Connection getConnection() {
		try{
	//Class.forName("com.mysql.jdbc.Driver"); 		
	String url       = "jdbc:mysql://localhost:3306/enterprise";
    String user      = "root";
    String password  = "";
    conn = DriverManager.getConnection(url, user, password);
		}
	catch (SQLException e) { System.out.println(e.getMessage());}
	return conn;
}
}	
