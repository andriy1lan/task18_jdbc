package dao;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;

import model.Department;

public class DepartmentDao implements DAO {
	      Connection con;
	
	public Department getById(int id) {
		  Department dep=null;
		  con = ConnectionFactory.getConnection(); 
		  try (Statement stm = con.createStatement(); 
		   ResultSet rs = stm.executeQuery("SELECT * FROM department WHERE department_id=" + id);) {
		   while (rs.next()) { 
		    dep= new Department(id, rs.getString("name"), rs.getString("address"));
		   }
		  } catch (SQLException e) {
		   e.printStackTrace();
		  }
		  try {
		   con.close();
		  } catch (SQLException ex) {
		  }
		  return dep;
		 }
	
	public List<Department> getAll() {
		  List<Department> deps = new ArrayList<>();
		  con = ConnectionFactory.getConnection(); // create connection
		  try (Statement stm = con.createStatement(); 
		   ResultSet rs = stm.executeQuery("SELECT * FROM department");) {
		   while (rs.next()) { 
		    deps.add(new Department(rs.getInt("department_id"), rs.getString("name"), rs.getString("address")));
		   }
		  } catch (SQLException e) {
		   e.printStackTrace();
		  }
		  try {
		   con.close();
		  } catch (SQLException ex) {
		  }
		  return deps;
		 }
	
	public void insert(Department dep) {
		   con = ConnectionFactory.getConnection(); 
		   try {
		   Statement stm = con.createStatement();
		   String query = "INSERT INTO department (name,address) VALUES ('" + dep.getName() + "','" + dep.getAddress() + "') ";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	public void update(Department dep) {
		   con = ConnectionFactory.getConnection(); 
		   try {
		   Statement stm = con.createStatement();
		   String query = "UPDATE department SET name='" + dep.getName() + "',address='" + dep.getAddress() + "'  WHERE department_id=" + dep.getDepartment_id() + " ;";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	public void delete(int id) {
		   con = ConnectionFactory.getConnection(); 
		   try {
		   Statement stm = con.createStatement();
		   String query = "DELETE FROM department WHERE department_id=" + id + " ;";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	//Delete department and move its employees to first existing one (if they exists) - change foreign key in employees table
	//Execute several queries in one transaction
	public void deleteMoveEmployees(int id) {
		   int newid=0;
		   con = ConnectionFactory.getConnection();
		   try {
		   con.setAutoCommit(false);
		   Statement stm = con.createStatement();
		   ResultSet rs = stm.executeQuery("SELECT department_department_id FROM employee");
		   while (rs.next()) {
			    if (rs.getInt("department_department_id")!=id) {
			    newid=rs.getInt("department_department_id"); break; }
			   }
		   String query = "UPDATE employee SET department_department_id='" + newid + "'  WHERE department_department_id=" + id + " ;";
		   stm.executeUpdate(query);
		   String query1 = "DELETE FROM department WHERE department_id=" + id + " ;";
		   stm.executeUpdate(query1);
		   con.commit();
		   con.setAutoCommit(true);
		   con.close();
		   if (newid==0) {System.out.print("No another department to trasfer demployees");}
		  } catch (SQLException ex) {
		  try {
			con.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		   ex.printStackTrace();
		  }
		 }
	
	public static void main(String[] args) {
		DepartmentDao dd=new DepartmentDao();
		Department d=dd.getById(2);
		System.out.println(d);
		List<Department> dl=dd.getAll();
		for(Department d0:dl) {System.out.println(d0);}
		//dd.insert(new Department("Forth",null));
		//dd.update(new Department(3,"Third","Lviv, Shevchenka 7"));
		//dd.deleteMoveEmployees(2);
	}
	}
	
