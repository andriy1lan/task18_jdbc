package dao;
import java.util.List;

public interface DAO<T> {
	
	  public abstract void insert(T obj);
	  public abstract T getById(int id);
	  public abstract void update(T obj);
	  public abstract void delete(int id);
	  public abstract List<T> getAll();

}
