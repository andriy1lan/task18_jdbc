package dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;


import model.Employee;
import model.Gender;


public class EmployeeDAO implements DAO {

Connection con;

    public Date convertStringToDate(String str) {
    	SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    	java.util.Date date=null;
		try {
			date = sdf1.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	java.sql.Date sqlDate = new java.sql.Date(date.getTime());
    	return sqlDate;
    }
	
	public Employee getById(int id) {
		  Employee emp=null;
		  con = ConnectionFactory.getConnection(); 
		  try (Statement stm = con.createStatement(); 
		   ResultSet rs = stm.executeQuery("SELECT * FROM employee WHERE employee_id=" + id);) {
		   while (rs.next()) { 
		    emp= new Employee (id, rs.getString("name"), rs.getString("surname"), rs.getString("middle_name"), rs.getDate("birthdate"), rs.getString("post"),Gender.valueOf(rs.getString("gender")), rs.getBigDecimal("salary"), rs.getString("address"), rs.getInt("department_department_id"));
		   }
		  } catch (SQLException e) {
		   e.printStackTrace();
		  }
		  try {
		   con.close();
		  } catch (SQLException ex) {
		  }
		  return emp;
		 }
	
	public List<Employee> getAll() {
		  List<Employee> emps = new ArrayList<>();
		  con = ConnectionFactory.getConnection(); 
		  try (Statement stm = con.createStatement(); 
		   ResultSet rs = stm.executeQuery("SELECT * FROM employee");) {
		   while (rs.next()) {
		    emps.add(new Employee (rs.getInt("employee_id"), rs.getString("name"), rs.getString("surname"), rs.getString("middle_name"), rs.getDate("birthdate"), rs.getString("post"), Gender.valueOf(rs.getString("gender")), rs.getBigDecimal("salary"), rs.getString("address"), rs.getInt("department_department_id")));
		   }
		  } catch (SQLException e) {
		   e.printStackTrace();
		  }
		  try {
		   con.close();
		  } catch (SQLException ex) {
		  }
		  return emps;
		 }
	
	public void insert(Employee emp) {
		   con = ConnectionFactory.getConnection(); 
		   try {
		   Statement stm = con.createStatement();
		   String query = "INSERT INTO employee (name,surname, middle_name, birthdate, post,gender, salary, address, department_department_id) VALUES ('" + emp.getName() + "','" + emp.getSurname() + "','"+emp.getMiddle_name()+"','"+emp.getDatebirth().toString()+"','"+emp.getPost()+"','"+emp.getGender().toString()+"','"+emp.getSalary().toString()+"','"+emp.getAddress()+"','"+emp.getDepartment_id()+"') ";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	public void update(Employee emp) {
		   con = ConnectionFactory.getConnection();
		   try {
		   Statement stm = con.createStatement();
		   //String query = "INSERT INTO employee (name,surname, middle_name, birthdate, post,gender, salary, address, department_department_id) VALUES ('" + emp.getName() + "','" + emp.getSurname() + "','"+emp.getMiddle_name()+"','"+emp.getDatebirth().toString()+"','"+emp.getPost()+"','"+emp.getGender().toString()+"','"+emp.getSalary().toString()+"','"+emp.getAddress()+"','"+emp.getDepartment_id()+"') ";
		   String query = "UPDATE employee SET name='" + emp.getName() + "',surname='" + emp.getSurname() + "',middle_name='" + emp.getMiddle_name() + "',birthdate='" + emp.getDatebirth().toString() + "',post='" + emp.getPost() + "', gender='" + emp.getGender().toString() + "',salary='" + emp.getSalary().toString() + "', address='" + emp.getAddress() + "',department_department_id='" + emp.getDepartment_id() + "'  WHERE employee_id=" + emp.getEmployee_id() + " ;";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	public void delete(int id) {
		   con = ConnectionFactory.getConnection();
		   try {
		   Statement stm = con.createStatement();
		   String query = "DELETE FROM employee WHERE employee_id=" + id + " ;";
		   stm.executeUpdate(query);
		   con.close();
		  } catch (SQLException ex) {
		   ex.printStackTrace();
		  }
		 }
	
	public static void main(String[] args) {
		EmployeeDAO ed=new EmployeeDAO();
		Employee e1=ed.getAll().get(0);
		Employee e2=ed.getById(1);
		System.out.println(e1);
		System.out.println(e2);
		//Employee e3=new Employee("Roman","Olexiv","Mykhailovych",new java.sql.Date(1980, 11, 10),"worker",Gender.MALE,new BigDecimal(13000.0),"Lviv, Zelena st. 110",3);
		//ed.insert(e3);
		//System.out.println(new java.sql.Date(2000-1900,01,11));
		//Employee e4=new Employee(3,"Roman","Olexiv","Mykhailovych",new java.sql.Date(1980-1900, 11, 10),"worker",Gender.MALE,new BigDecimal(13000.0),"Lviv, Zelena st. 110",3);
		//ed.delete(5);
	}

}
