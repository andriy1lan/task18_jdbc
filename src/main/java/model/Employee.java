package model;

import java.math.BigDecimal;
import java.sql.Date;

public class Employee {
	
	int employee_id;
	String name;
	String surname;
	String middle_name;
	Date datebirth;
	String post;
	Gender gender;
	BigDecimal salary;
	String address;
    int department_id;
    
    public Employee(String name, String surname,
			String middle_name, Date datebirth, String post, Gender gender,
			BigDecimal salary, String address, int department_id) {
		this.name = name;
		this.surname = surname;
		this.middle_name = middle_name;
		this.datebirth = datebirth;
		this.post = post;
		this.gender = gender;
		this.salary = salary;
		this.address = address;
		this.department_id = department_id;
	}
    
	public Employee(int employee_id, String name, String surname,
			String middle_name, Date datebirth, String post, Gender gender,
			BigDecimal salary, String address, int department_id) {
		super();
		this.employee_id = employee_id;
		this.name = name;
		this.surname = surname;
		this.middle_name = middle_name;
		this.datebirth = datebirth;
		this.post = post;
		this.gender = gender;
		this.salary = salary;
		this.address = address;
		this.department_id = department_id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public Date getDatebirth() {
		return datebirth;
	}

	public String getPost() {
		return post;
	}

	public Gender getGender() {
		return gender;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public String getAddress() {
		return address;
	}

	public int getDepartment_id() {
		return department_id;
	}

	@Override
	public String toString() {
		return "Employee [employee_id=" + employee_id + ", name=" + name
				+ ", surname=" + surname + ", middle_name=" + middle_name
				+ ", datebirth=" + datebirth + ", post=" + post + ", gender="
				+ gender + ", salary=" + salary + ", address=" + address
				+ ", department_id=" + department_id + "]";
	}
	
	
    
}
