package model;

public class Department {
	
	int department_id;
	String name;
	String address;
	
	public Department(String name, String address) {
		this.name = name;
		this.address = address;
	}
	
	public Department (int department_id, String name, String address) {
		this.department_id = department_id;
		this.name = name;
		this.address = address;
	}

	public int getDepartment_id() {
		return department_id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "Department [department_id=" + department_id + ", name=" + name
				+ ", address=" + address + "]";
	}
	
	
}
