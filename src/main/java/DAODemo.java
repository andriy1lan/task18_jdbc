import java.math.BigDecimal;
import java.util.List;

import dao.DepartmentDao;
import dao.EmployeeDAO;

import model.Department;
import model.Employee;
import model.Gender;

public class DAODemo {

	public static void main(String[] args) {
		//1.Check the DAO for department table
		DepartmentDao dd=new DepartmentDao();
		Department d=dd.getById(3);
		System.out.println(d);
		List<Department> dl=dd.getAll();
		for(Department d0:dl) {System.out.println(d0);}
		//dd.insert(new Department("Forth",null));
		//dd.update(new Department(3,"Third","Lviv, Shevchenka st. 7"));
		//dd.deleteMoveEmployees(2);
		
		//2.Check the DAO for employee table
		EmployeeDAO ed=new EmployeeDAO();
		Employee e1=ed.getAll().get(0);
		Employee e2=ed.getById(2);
		System.out.println(e1);
		System.out.println(e2);
		//Employee e3=new Employee("Roman","Olexiv","Mykhailovych",new java.sql.Date(1980, 11, 10),"worker",Gender.MALE,new BigDecimal(13000.0),"Lviv, Zelena st.110",3);
		//ed.insert(e3);
		//Employee e4=new Employee(3,"Roman","Olexiv","Mykhailovych",new java.sql.Date(1980-1900, 11, 10),"worker",Gender.MALE,new BigDecimal(13000.0),"Lviv, Zelena st.110",3);
		//ed.update(e4);
		//ed.delete(6);
	}
}
